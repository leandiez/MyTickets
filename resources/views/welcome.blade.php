@extends('layouts.app')

@section('content')
<div class="card text-white bg-primary mb-3" style="max-width: 100%;">
    <div class="card-header">Pagina Inicial</div>
    <div class="card-body">
        <h4 class="card-title">Bienvenidos a MyTickets</h4>
        <p class="card-text">Este es un ejemplo de texto para la pagina inicial de mi sistema.</p>
    </div>
</div>
@endsection
