<div class="card">
    <div class="card-header">Menu</div>
    <div class="card-info">
    @if(auth()->check())
	<ul class="nav nav-pills flex-column">
	  <li class="nav-item">
	    <a class="nav-link active" href="#">Dashboard</a>
	  </li>
	  <li class="nav-item dropdown">
	    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Incidencias</a>
	    <div class="dropdown-menu">
	      <a class="dropdown-item" href="#">Ver Incidencias</a>
	      <a class="dropdown-item" href="#">Reportar Incidencia</a>
	      <div class="dropdown-divider"></div>
	      <a class="dropdown-item" href="#">Separated link</a>
	    </div>
	  </li>
	  <li class="nav-item dropdown">
	    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Administracion</a>
	    <div class="dropdown-menu">
	      <a class="dropdown-item" href="#">Usuarios</a>
	      <a class="dropdown-item" href="#">Proyectos</a>
	      <a class="dropdown-item" href="#">Configuracion</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link disabled" href="#">Disabled</a>
	  </li>
	</ul>
	@else
	<ul class="nav nav-pills flex-column">
	  <li class="nav-item">
	    <a class="nav-link active" href="#">Bienvenido</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" href="#">Instrucciones</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link disabled" href="#">Sobre Este Sitio</a>
	  </li>
	</ul>
	@endif
    </div>
</div>
